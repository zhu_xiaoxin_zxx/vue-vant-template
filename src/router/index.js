import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'hash',
  routes: [{
    path: '/login',
    name: 'Login',
    component: () => import('@/views/Login')
  }, {
    path: '/',
    name: 'Home',
    component: () => import('@/views/Home')
  }, {
    path: '*',
    component: () => import('@/views/NotFound')
  }]
});

export default router;