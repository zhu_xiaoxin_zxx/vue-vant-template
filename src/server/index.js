import axios from "axios";
import router from "../router";

const service = axios.create({
  timeout: 30 * 1000,
  withCredentials: true,
});

axios.interceptors.request.use(
  function (config) {
    window.$Spin.show();
    return config;
  },
  function (error) {
    window.$Spin.hide();
    return Promise.reject(error);
  }
);

axios.interceptors.response.use(
  function (response) {
    window.$Spin.hide();
    if (response.data.code === -1 && router.currentRoute.path !== "/login") {
      localStorage.setItem("target_uri", router.currentRoute.fullPath);
      router.replace("/login");
    }
    return response;
  },
  function (error) {
    window.$Spin.hide();
    return Promise.reject(error);
  }
);

export default service;
