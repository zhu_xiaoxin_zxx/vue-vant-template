import Vue from "vue";
import router from "./router";
import "./util/enhance";
import "./main.css";
import apis from "./api";
import store from "./store";
import Vant from "vant";
import "vant/lib/index.css";
import "vant/lib/icon/local.css";
import App from "./App";
import service from "./server";

Vue.config.productionTip = false;
Vue.prototype.$http = service;
Vue.prototype.APIS = apis;
Vue.use(Vant);

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
