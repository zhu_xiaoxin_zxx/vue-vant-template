module.exports = {
  publicPath: '/mobile/',
  filenameHashing: true,
  productionSourceMap: false,
  devServer: {
    open: true,
    port: 8090,
    proxy: {
      '/api': {
        target: 'http://localhost:3000',
        changeOrigin: true,
        ws: true
      }
    },
  }
};